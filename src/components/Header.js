import React, {Component} from 'react';
import {Link, NavLink} from "react-router-dom";
import { Container, Visibility, Menu, Icon } from 'semantic-ui-react';

class Header extends Component {

    render() {
        return (
            <Visibility>
                <Menu borderless>
                    <Container text>
                        <Menu.Item as={Link} to="/" exact="true">
                            <Icon size='large' name='thumbs up outline' />
                            <Menu.Item header>LinkVote App</Menu.Item>
                        </Menu.Item>
                        <Menu.Item position='right' as={NavLink} to="/add-new">
                            <Icon size='small' name='add' />
                            Add New
                        </Menu.Item>
                    </Container>
                </Menu>
            </Visibility>
        );
    }
}

export default Header;
