import React from 'react';
import 'semantic-ui-css/semantic.min.css';
import './styles/styles.scss';

import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';


import {Container} from 'semantic-ui-react';

import Header from "./components/Header";
import Home from "./components/pages/Home";
import AddNew from "./components/pages/AddNew";

import {Route} from "react-router-dom";

class App extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      id: "",
      title: "",
      url: "",
      score: 0,
      items : JSON.parse(window.localStorage.getItem('items')) || [],
    };
  }

  render(){
    return(
        <div className="App">
            <ToastContainer />
            <Header />
            <Container text>
                <Route exact path='/' component={Home} />
                <Route exact path='/add-new' component={AddNew}/>
            </Container>
        </div>
    );
  }
}

export default App;
