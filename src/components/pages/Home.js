import React, {Component} from 'react';
import _ from 'lodash';
import Modal from 'react-modal';
import { Pagination } from 'semantic-ui-react';
import SortOptions from "../SortOptions";
import ItemList from "../ItemList";
import { initialData } from "../../data/InitialData";
import { toast } from "react-toastify";

const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    }
};

class Home extends Component {

    constructor(props) {
        if (process.env.NODE_ENV !== 'test') Modal.setAppElement('#root');

        super(props);
        this.state = {
            activePage: 1,
            orderBy: window.localStorage.getItem('orderBy') || 'time',
            orderType: window.localStorage.getItem('orderType') || 'desc',
        };
        this.setLocalStorage();


        this.openRemoveModal = this.openRemoveModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    setLocalStorage = () => {
        const localStorageData = this.getLocalStorage();
        const data = localStorageData || initialData;

        window.localStorage.setItem('items', JSON.stringify(data));
    };

    getLocalStorage = () => {
        return JSON.parse(window.localStorage.getItem('items'))
    };

    updateScore = (index, val) => {
        let updatedLinks = this.getLocalStorage().slice();
        const {orderBy, orderType, activePage } = this.state;

        index = (activePage - 1) * 5 + index;

        updatedLinks[index].score += val;
        window.localStorage.setItem('items', JSON.stringify(updatedLinks));
        this.updateOrder(orderBy, orderType);
    };

    removeItem = () => {
        var tempLinks = this.getLocalStorage().slice();
        const {orderBy, orderType, removeAble } = this.state;

        tempLinks.splice(removeAble, 1);
        this.updateOrder(orderBy, orderType);
        window.localStorage.setItem('items', JSON.stringify(tempLinks));
        toast.success(`${this.state.modalTitle} is removed`);

        this.closeModal();
    };

    updateOrder = (orderBy = 'time', orderType = 'desc', setPage = null) => {
        let data = this.getLocalStorage().slice();
        window.localStorage.clear('items');

        if (setPage) this.setState({ activePage: setPage });

        this.setState({ orderBy: orderBy, orderType: orderType });
        data = _.orderBy(data, [orderBy, 'voted_at'], [orderType, 'desc']);

        window.localStorage.setItem('items', JSON.stringify(data));
    };

    setPageNum = (event, { activePage }) => {
        this.setState({ activePage: activePage });
    };

    openRemoveModal = (index) => {
        let data = this.getLocalStorage().slice();

        this.setState({modalIsOpen: true, removeAble: index, modalTitle:  data[index].title });
    };

    afterOpenModal = () => {
        this.subtitle = 'Reddit';
    };

    closeModal = () => {
        this.setState({modalIsOpen: false});
    };

    render() {
        const {orderBy, orderType, activePage } = this.state;
        const itemsPerPage = 5;
        const totalPages = this.getLocalStorage().length / itemsPerPage;
        const items = this.getLocalStorage().slice(
            (activePage - 1) * itemsPerPage,
            (activePage - 1) * itemsPerPage + itemsPerPage
        );

        const pageContent = (<div className="homeContainer">
            <Modal
                isOpen={this.state.modalIsOpen}
                onAfterOpen={this.afterOpenModal}
                onRequestClose={this.closeModal}
                style={customStyles}
            >
                <h2>Remove Link</h2>
                <div style={{margin: '20px 0'}}>
                    Do you want to remove <strong>{this.state.modalTitle}?</strong>
                </div>
                <div className="modalConfirm">
                    <button onClick={this.removeItem.bind(this)}>OK</button>
                    <button onClick={this.closeModal}>Cancel</button>
                </div>
            </Modal>
            <SortOptions updateOrder = {this.updateOrder.bind(this)} />
            <ItemList itemListData = {_.orderBy(items, [orderBy, 'voted_at'], [orderType, 'desc'])}
                      updateScore = {this.updateScore.bind(this)}
                      removeItem = {this.openRemoveModal} />
            <Pagination
                activePage={activePage}
                onPageChange={this.setPageNum.bind(this)}
                totalPages={totalPages}
                ellipsisItem={null}
            />
        </div>);

        const emptyMessage = (
            <p>There are no movies yet.</p>
        );

        return (
            <div>
                {items.length === 0 ? emptyMessage : pageContent }
            </div>
        );
    }
}

export default Home;
