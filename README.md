
## React LinkVote Application
:boom: **npm install** or  **yarn install** to install dependencies.
:rocket: **npm start** or **yarn start** to start web application.

This is react **LinkVote** application for playground. It uses LocalStorage for data storage and data loading. I used Semantic UI for front-end components.

It based on two pages. It uses React Router for routing:

 1. **List Page** 
List page is home of application which lists all links in it. It has pagination per 5 links.  You can vote links with :thumbsup: and :thumbsdown:

You can remove link when you click delete icon on hover item. There is a modal for confirmation.

By default, sorting option is by date. You can choose your sorting options. 
You have two options for sorting: 

	 1. Most Voted (Z-A): It lists most voted links on top to bottom.
	 2. Less Voted (A-Z): It lists less voted links on top to bottom.

If voted scores are equal then last voted item will be first.

 2. **Adding New Link Page**
This page is for adding new links. You'll notify with toast messages when you succeed and has errors. Form has simple validations.
  
## Available Scripts  
In the project directory, you can run:  
  
### `yarn start`  
  
Runs the app in the development mode.<br />  
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.  
  
The page will reload if you make edits.<br />  
You will also see any lint errors in the console.  
  
### `yarn test`  
  
Application has 6 test suites. Totally LinkVote has 9 tests.
Launches the test runner in the interactive watch mode. 
