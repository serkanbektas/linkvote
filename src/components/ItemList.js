import React from 'react';
import PropTypes from 'prop-types';
import Post from "./Post";
import {List} from 'semantic-ui-react';


const ItemList = props => {
    return (
        <List columns={1}>
            {
                props.itemListData.map((item, index) => (
                    <Post key = {index}
                          title = {item.title}
                          url = {item.url}
                          score = {item.score}
                          incrementScore = {() => props.updateScore(index, 1)}
                          decrementScore = {() => props.updateScore(index, -1)}
                          removeItem = {() => props.removeItem(index)}
                    />
                ))
            }
        </List>
    )
};

ItemList.propTypes = {
    itemListData: PropTypes.array.isRequired,
    updateScore: PropTypes.func.isRequired,
    removeItem: PropTypes.func.isRequired,
};

export default ItemList;
