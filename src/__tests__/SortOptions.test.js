import React from 'react';
import ReactDOM from "react-dom";
import {Dropdown} from 'semantic-ui-react';

describe('<Dropdown /> component', () => {

    it("list component renders without crashing", () => {
        const div = document.createElement("div");
        ReactDOM.render(
            <Dropdown />
            , div);
        ReactDOM.unmountComponentAtNode(div);
    });
});
