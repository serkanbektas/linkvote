import React from 'react';
import PropTypes from 'prop-types';
import { List, Button, Label } from 'semantic-ui-react'

const Post = props => {

    return (
        <List.Item>
            <List.Content>
                <Label>{props.score} points</Label>
            </List.Content>
            <List.Content>
                <List.Header>{props.title}</List.Header>
                <List.Description>({props.url})</List.Description>
                    <Button onClick={() => props.removeItem()} circular icon="delete" className="remove" color="red" />
                    <Button circular icon="arrow up" onClick={() => props.incrementScore()}/>
                    <Button circular icon="arrow down" onClick={() => props.decrementScore()}/>
            </List.Content>
        </List.Item>
    );
};

Post.propTypes = {
    removeItem: PropTypes.func.isRequired,
    incrementScore: PropTypes.func.isRequired,
    decrementScore: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    score: PropTypes.number.isRequired,
    url: PropTypes.string.isRequired,
};

export default Post;
