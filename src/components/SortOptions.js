import React from 'react';
import PropTypes from 'prop-types';
import {Dropdown} from 'semantic-ui-react';

const SortOptions = props => {

    const handleOnChange = (e, data) => {
        props.updateOrder('score', data.value, 1);

        window.localStorage.setItem('orderBy', 'score');
        window.localStorage.setItem('orderType', data.value);
    };

    const defaultOption = 'Order by';
    const firstOption = 'Most Voted (Z-A)';
    const secondOption = 'Less Voted (A-Z)';

    const options = [
        { key: 1, text: firstOption, value: 'desc' },
        { key: 2, text: secondOption, value: 'asc' },
    ];

    const orderByOption = window.localStorage.getItem('orderBy');
    const orderTypeOption = window.localStorage.getItem('orderType');
    const placeHolder = !orderByOption ? defaultOption
                        : orderTypeOption === 'desc' ?  firstOption : secondOption;

    return (
        <Dropdown
            placeholder={placeHolder}
            selection
            options={options}
            onChange={handleOnChange}
        />
    )
};

SortOptions.propTypes = {
    updateOrder: PropTypes.func.isRequired,
};
export default SortOptions;
