import React, { Component } from 'react';
import {Button, Form} from "semantic-ui-react";
import { initialData } from "../../data/InitialData";

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';

class AddNew extends Component {

    constructor(props) {
        super(props);
        this.state = {
            items: JSON.parse(window.localStorage.getItem('items')) || initialData,
        }
    }

    handleChange = (e) => {

        const errors = this.validate();

        this.setState({
            errors,
            [e.target.name]: e.target.value
        });
    };

    onAdd = () => {
        const errors = this.validate();
        this.setState({
            errors,
            redirect: true
        });
        if (Object.keys(errors).length === 0) {
            let {title, url } = this.state;
            let temporaryLinks = this.state.items.slice();

            temporaryLinks.push({"title": title, 'url': url, "score":0, time: Date.now(), voted_at: ''});
            temporaryLinks.sort((a,b) => {
                return b.score - a.score;
            });

            toast.success(`${title} is added successfully.`);

            this.setState({
                items: temporaryLinks,
                title:'',
                url: '',
            });

            window.localStorage.setItem('items', JSON.stringify(temporaryLinks));
        } else {
            toast.error(`Please fill all inputs!`);
        }
    };

    validate = () => {
        const errors = {};
        if (!this.state.title) errors.title = true;
        if (!this.state.url) errors.url = true;
        return errors;
    };

    render() {
        return(
            <Form className="newForm">
                <Form.Field>
                    <label htmlFor="title">Link Name:</label>
                    <input name="title" id="title" value={this.state.title} onChange={this.handleChange.bind(this)} />
                </Form.Field>
                <Form.Field>
                    <label htmlFor="url">Link URL:</label>
                    <input name="url" id="url" value={this.state.url} onChange={this.handleChange.bind(this)} />
                </Form.Field>
                <Button primary onClick={this.onAdd}>Add</Button>
            </Form>
        );
    }
}

export default AddNew;
