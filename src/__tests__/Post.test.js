import React from "react";
import ReactDOM from "react-dom";
import { List, Button, Label } from 'semantic-ui-react'

it("list component renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(
        <List />
        , div);
    ReactDOM.unmountComponentAtNode(div);
});
