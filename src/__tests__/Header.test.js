import React from "react";
import ReactDOM from "react-dom";
import { Container, Visibility, Menu } from 'semantic-ui-react';

it("visibility renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(
        <Visibility />
        , div);
    ReactDOM.unmountComponentAtNode(div);
});

it("menu renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(
        <Visibility>
            <Menu />
        </Visibility>
        , div);
    ReactDOM.unmountComponentAtNode(div);
});

it("menu container renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(
        <Visibility>
            <Menu>
                <Container />
            </Menu>
        </Visibility>
        , div);
    ReactDOM.unmountComponentAtNode(div);
});

it("menu item renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(
        <Visibility>
            <Menu>
                <Container>
                    <Menu.Item />
                </Container>
            </Menu>
        </Visibility>
        , div);
    ReactDOM.unmountComponentAtNode(div);
});
